function kiem_tra_snt(n) {
  var flag = true;
  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    for (var i = 3; i <= Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }
  return flag;
}

var numberArr = [];
var number2Arr = [];
function btnKiemTra() {
  var number = document.querySelector("#txt-number").value * 1;
  numberArr.push(number);
  console.log("numberArr: ", numberArr);
  var soDuong = 0;
  var soAm = 0;
  var tongSoDuong = 0;
  var soNhoNhat = numberArr[0];
  var soLonNhat = numberArr[0];
  var soDuongNhoNhat = numberArr[0];
  var soChanCuoiCung = numberArr[0];
  var soNTDauTien = numberArr[0];

  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      soDuong++;
      tongSoDuong = tongSoDuong + currentNumber;
    }
    if (currentNumber < 0) {
      soAm++;
    }
    if (currentNumber < soNhoNhat) {
      soNhoNhat = currentNumber;
    }
    if (currentNumber > 0 && currentNumber < soDuongNhoNhat) {
      soDuongNhoNhat = currentNumber;
    }
    if (currentNumber % 2 == 0) {
      soChanCuoiCung = currentNumber;
    }
  }
  for (index = 0; index < numberArr.length; index++) {
    if (
      kiem_tra_snt(currentNumber) &&
      currentNumber > 0 &&
      currentNumber < soNhoNhat
    ) {
      soNTDauTien = currentNumber;
      break;
    }
  }
  document.getElementById(
    "arraySoNguyen"
  ).innerHTML = `<div> ${numberArr} </div>`;
  document.getElementById(
    "result"
  ).innerHTML = `<div>Tổng các số dương có trong mảng: ${tongSoDuong} </div>
  <div>Có tất cả ${soDuong} số dương trong mảng </div>
  <div>Số nhỏ nhất trong mảng là ${soNhoNhat} </div>
  <div>Số dương nhỏ nhất trong mảng là ${soDuongNhoNhat} </div>
  <div>Số chẵn cuối cùng trong mảng là ${soChanCuoiCung} </div>
  <div>Số nguyên tố đầu tiên trong mảng là ${soNTDauTien} </div>`;
}
function btnSapXep() {
  var number = document.querySelector("#txt-number").value * 1;
  numberArr.sort();
  document.getElementById(
    "result-sapxep"
  ).innerHTML = `<div>Mảng sau khi sắp xếp ${numberArr} </div>`;
}
function btnSoSanh() {
  var number = document.querySelector("#txt-number").value * 1;
  var soDuong = 0;
  var soAm = 0;
  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      soDuong++;
    }
    if (currentNumber < 0) {
      soAm++;
    }
  }
  if (soAm < soDuong) {
    document.getElementById("result-sosanh").innerHTML = `Số Âm < Số Dương`;
  } else {
    document.getElementById("result-sosanh").innerHTML = `Số Âm > Số Dương`;
  }
}

function btnDoiViTri() {
  var number = document.querySelector("#txt-number").value * 1;
  var vitri1 = document.getElementById("txt-vitri1").value * 1;
  var vitri2 = document.getElementById("txt-vitri2").value * 1;
  numberArr[vitri1];
  numberArr[vitri2];
  var bienTam = numberArr[vitri1];
  numberArr[vitri1] = numberArr[vitri2];
  numberArr[vitri2] = bienTam;
  document.getElementById(
    "result-doivitri"
  ).innerHTML = `<div>Mảng sau khi đổi vị trí ${numberArr} </div>`;
}
function btnTimSoNguyen() {
  var number = document.querySelector("#txt-number2").value * 1;
  number2Arr.push(number);
  var soNguyen = 0;
  for (var index = 0; index < number2Arr.length; index++) {
    var currentNumber = number2Arr[index];
    if (Number.isInteger(currentNumber) == true) {
      soNguyen++;
    }
  }
  document.getElementById(
    "result-array2"
  ).innerHTML = `<div> ${number2Arr} </div>`;
  document.getElementById(
    "result-songuyen"
  ).innerHTML = `<div>Số nguyên có trong mảng là ${soNguyen} </div>`;
}
